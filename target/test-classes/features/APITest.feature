Feature: Request to todo.ly

  @Fast
  Scenario: User creation on the platform
    Given The user is found in the path to create user
    When User completes the form
    Then All user information is displayed to validate the user creation

  @Slow
  Scenario: Creating a user with an existing email address
    Given The user is found in the path to create user
    When The user completes the form with an existing email address
    Then A message is displayed indicating an error

  @Slow
  Scenario: Creating a user with an invalid email address format
    Given The user is found in the path to create user
    When The user completes the form with an invalid email address format
    Then A message is displayed indicating an error

  @Fast
  Scenario: Verify if the user is authenticated
    Given The user is in the path to verify its authenticity
    When Enter the username and password of the user to be verified
    Then A message is displayed indicating whether it is true or false

  @Fast
  Scenario: Get the user token
    Given The user is in the path to verify its authenticity
    When Enter the username and password of the user to get the token
    Then A message is displayed indicating the token of the user

  @Fast
  Scenario: Get user information
    Given The user is in the path to verify its authenticity
    When Enter the username and password of the user to get information
    Then All the information of user is displayed

  @Fast
  Scenario: Update a user's email
    Given The user is in the path to verify its authenticity
    When Enter the new email to update
    Then All updated information is displayed

  @Fast
  Scenario: Deleting a current user
    Given The user is in the path to verify its authenticity
    When Enter the username and password of the user to delete
    Then A message is displayed with the message null