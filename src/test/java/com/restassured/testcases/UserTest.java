package com.restassured.testcases;

import io.cucumber.java.en.*;
import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;

import java.io.File;
import java.util.Base64;

import static io.restassured.RestAssured.*;

public class UserTest {
    private Response response;
    public String userName = "usertest135@mail.com";
    public String updatedUserName = "updatedUser9@mail.es";
    public String fullName = "Karl Moore";
    public String password = "pASswoRd";

    public static String encode(String mail, String pass){
        return new String (Base64.getEncoder().encode((mail+":"+pass).getBytes()));
    }

    @Given("The user is found in the path to create user")
    public void theUserIsFoundInThePathToCreateUser() {
        RestAssured.baseURI = "https://todo.ly";
        RestAssured.basePath = "/api";
    }

    @When("User completes the form")
    public void userCompletesTheForm() {

        File file = new File("C:\\Users\\artro\\IdeaProjects\\projectREST\\src\\test\\java\\Data\\schemaResponse.json");

        response = given()
                .body("{\n" +
                        "  \"Email\": \""+userName+"\",\n" +
                        "  \"FullName\": \""+fullName+"\",\n" +
                        "  \"Password\": \""+password+"\"\n" +
                        "}").log().all()
                .post("/user.json")
                .then().log().all()
                .extract().response();

        var bodyResponse = response.getBody().asString();

        MatcherAssert.assertThat(bodyResponse, JsonSchemaValidator.matchesJsonSchema(file));

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals(fullName, response.jsonPath().getString("FullName"));
        Assertions.assertEquals(userName, response.jsonPath().getString("Email"));
        Assertions.assertNotEquals(password, response.jsonPath().getString("Password"));
    }

    @Then("All user information is displayed to validate the user creation")
    public void allUserInformationIsDisplayedToValidateTheUserCreation() {
        System.out.println(response.prettyPrint());
    }

    @When("The user completes the form with an existing email address")
    public void theUserCompletesTheFormWithAnExistingEmailAddress() {

        File file = new File("C:\\Users\\artro\\IdeaProjects\\projectREST\\src\\test\\java\\Data\\schemaError.json");

        response = given()
                .body("{\n" +
                        "  \"Email\": \"vasdGvas12@email.es\",\n" +
                        "  \"FullName\": \""+fullName+"\",\n" +
                        "  \"Password\": \""+password+"\"\n" +
                        "}")
                .post("/user.json")
                .then()
                .extract().response();

        var bodyResponse = response.getBody().asString();

        MatcherAssert.assertThat(bodyResponse, JsonSchemaValidator.matchesJsonSchema(file));

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals(201, response.jsonPath().getInt("ErrorCode"));
        Assertions.assertEquals("Account with this email address already exists", response.jsonPath().getString("ErrorMessage"));

    }

    @When("The user completes the form with an invalid email address format")
    public void theUserCompletesTheFormWithAnInvalidEmailAddressFormat() {

        File file = new File("C:\\Users\\artro\\IdeaProjects\\projectREST\\src\\test\\java\\Data\\schemaError.json");

        response = given()
                .body("{\n" +
                        "  \"Email\": \"mmmmm\",\n" +
                        "  \"FullName\": \""+fullName+"\",\n" +
                        "  \"Password\": \""+password+"\"\n" +
                        "}")
                .post("/user.json")
                .then()
                .extract().response();

        var bodyResponse = response.getBody().asString();

        MatcherAssert.assertThat(bodyResponse, JsonSchemaValidator.matchesJsonSchema(file));

        Assertions.assertEquals(200,response.statusCode());
        Assertions.assertEquals(307,response.jsonPath().getInt("ErrorCode"));
        Assertions.assertEquals("Invalid Email Address", response.jsonPath().getString("ErrorMessage"));

    }

    @Then("A message is displayed indicating an error")
    public void aMessageIsDisplayedIndicatingAnError() {

        System.out.println(response.prettyPrint());
    }

    @Given("The user is in the path to verify its authenticity")
    public void theUserIsInThePathToVerifyItsAuthenticity() {

        RestAssured.baseURI = "https://todo.ly";
        RestAssured.basePath = "/api";

    }

    @When("Enter the username and password of the user to be verified")
    public void enterTheUsernameAndPasswordOfTheUserToBeVerified() {

        String authorization = encode(userName, password);

        response =
                given()
                        .headers("Authorization", "Basic " + authorization)
                        .when()
                        .get("/authentication/isauthenticated.json")
                        .then()
                        .extract().response();

        Assertions.assertEquals(200, response.statusCode());

    }

    @Then("A message is displayed indicating whether it is true or false")
    public void aMessageIsDisplayedIndicatingWhetherItIsTrueOrFalse() {

        System.out.println(response.prettyPrint());
    }

    @When("Enter the username and password of the user to get the token")
    public void enterTheUsernameAndPasswordOfTheUserToGetTheToken() {

        File file = new File("C:\\Users\\artro\\IdeaProjects\\projectREST\\src\\test\\java\\Data\\schemaToken.json");

        String authorization = encode(userName, password);

        response =
                given()
                        .headers("Authorization", "Basic " + authorization)
                        .when()
                        .get("authentication/token.json")
                        .then().extract().response();

        var bodyResponse = response.getBody().asString();

        MatcherAssert.assertThat(bodyResponse, JsonSchemaValidator.matchesJsonSchema(file));

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertNotNull("TokenString");
        Assertions.assertNotNull("UserEmail");
        Assertions.assertNotNull("ExpirationTime");

        System.out.println(response.prettyPrint());

    }

    @Then("A message is displayed indicating the token of the user")
    public void aMessageIsDisplayedIndicatingTheTokenOfTheUser() {

        System.out.println(response.prettyPrint());

    }

    @When("Enter the username and password of the user to get information")
    public void enterTheUsernameAndPasswordOfTheUserToGetInformation() {

        File file = new File("C:\\Users\\artro\\IdeaProjects\\projectREST\\src\\test\\java\\Data\\schemaResponse.json");

        String authorization = encode(userName, password);

        response = given()
                .headers("Authorization", "Basic " + authorization)
                .when()
                .get("user.json")
                .then()
                .extract()
                .response();

        var bodyResponse = response.getBody().asString();

        MatcherAssert.assertThat(bodyResponse, JsonSchemaValidator.matchesJsonSchema(file));

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertNotNull(response.jsonPath().getInt("Id"));
        Assertions.assertEquals(fullName, response.jsonPath().getString("FullName"));
        Assertions.assertEquals(userName, response.jsonPath().getString("Email"));
        Assertions.assertNotEquals(password, response.jsonPath().getString("Password"));

    }

    @Then("All the information of user is displayed")
    public void allTheInformationOfUserIsDisplayed() {

        System.out.println(response.prettyPrint());

    }

    @When("Enter the new email to update")
    public void enterTheNewEmailToUpdate() {

        File file = new File("C:\\Users\\artro\\IdeaProjects\\projectREST\\src\\test\\java\\Data\\schemaResponse.json");

        String authorization = encode(userName, password);

        response = given()
                .headers("Authorization", "Basic " + authorization)
                .when()
                .body("{\n" +
                        "  \"Email\": \""+updatedUserName+"\",\n" +
                        "}")
                .put("/user/0.json")
                .then()
                .extract()
                .response();

        var bodyResponse = response.getBody().asString();
        MatcherAssert.assertThat(bodyResponse, JsonSchemaValidator.matchesJsonSchema(file));

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertNotNull(response.jsonPath().getInt("Id"));
        Assertions.assertEquals(fullName, response.jsonPath().getString("FullName"));
        Assertions.assertEquals(updatedUserName, response.jsonPath().getString("Email"));
        Assertions.assertNotEquals(password, response.jsonPath().getString("Password"));

    }

    @Then("All updated information is displayed")
    public void allUpdatedInformationIsDisplayed() {

        System.out.println(response.prettyPrint());

    }

    @When("Enter the username and password of the user to delete")
    public void enterTheUsernameAndPasswordOfTheUserToDelete() {

        File file = new File("C:\\Users\\artro\\IdeaProjects\\projectREST\\src\\test\\java\\Data\\schemaResponse.json");

        String authorization = encode(updatedUserName, password);

        Response response = given()
                .headers("Authorization", "Basic " + authorization)
                .when()
                .delete("/user/0.json")
                .then()
                .extract()
                .response();

        var bodyResponse = response.getBody().asString();
        MatcherAssert.assertThat(bodyResponse, JsonSchemaValidator.matchesJsonSchema(file));

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertNotNull(response.jsonPath().getInt("Id"));
        Assertions.assertEquals(fullName, response.jsonPath().getString("FullName"));
        Assertions.assertEquals(updatedUserName, response.jsonPath().getString("Email"));
        Assertions.assertNotEquals(password, response.jsonPath().getString("Password"));

    }

    @Then("A message is displayed with the message null")
    public void aMessageIsDisplayedWithTheMessageNull() {

        System.out.println(response);

    }
}
