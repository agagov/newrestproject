package Runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty"}, dryRun = true,
        features = "src/test/resources/features",
        glue = "com/restassured/testcases"
)

public class runnerTest {

}
